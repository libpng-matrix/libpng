FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > libpng.log'

COPY libpng .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' libpng
RUN bash ./docker.sh

RUN rm --force --recursive libpng
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD libpng
